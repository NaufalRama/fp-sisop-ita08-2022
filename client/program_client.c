// buat client

#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
  
int main(int argc, char const *argv[]) {
	char username[1024];
    bzero(username, sizeof(username));
    char password[1024];
    bzero(password, sizeof(password));
    char error[1024];
    bzero(error, sizeof(error));
    int valsend;
    int valread;
	char *hello = "Hello from client";
    char buffer[1024] = {0};
	

	if (argc == 1 && geteuid() == 0) {
        strcpy(username, "root");
        strcpy(password, "root");
    } else if (argc == 5) {
        strcpy(username, argv[2]);
        strcpy(password, argv[4]);
    } else {
        strcpy(error, "[+] Usage: ./client -u username -p password");
        printf("%s", error);
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;

    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n[-] Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\n[-] Invalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\n[-] Connection Failed \n");
        return -1;
    }

    // send(sock , hello , strlen(hello) , 0 );
    // printf("Hello message sent\n");
    // valread = read( sock , buffer, 1024);
    // printf("%s\n",buffer );

	bzero(buffer, sizeof(buffer));
	// send username to database
    valsend = send(sock, username, strlen(username), 0);
    if(valsend < 0)
    {
        printf("[-] Send Error");
    }
    sleep(1);
	// send password to databes
    valsend = send(sock, password, strlen(password), 0);
    if (valsend < 0)
    {
        printf("[-] Send Error");
    }
    
	// read response database
    valread = read(sock, buffer, 1024);
    if (valread < 0)
    {
        printf("[-] Read Error");
    }
    
    printf("%s\n", buffer);
    if (strncmp(buffer, "[+] Login Success", 13) != 0) {
        close(sock);
        return 0;
    }

    return 0;
}
